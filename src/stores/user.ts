import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const users = ref<User[]>([])
  const loadingStore = useLoadingStore()
async function getUser(id:number) {
    loadingStore.doload()
    const res = await userService.getUser(id)
    users.value =  res.data
    loadingStore.fisnish()
}
async function getUsers() {
    loadingStore.doload()
    const res = await userService.getUsers()
    users.value =  res.data
    loadingStore.fisnish()
}
async function saveUser(user:User){
    loadingStore.doload()

  if(user.id < 0){ //add New
    console.log('Post'+JSON.stringify(user));
    
    const res = await userService.addUser(user)
  }
  else{ //update
    console.log('Patch'+ JSON.stringify(user));
    
      const res = await userService.updateUser(user)
  }
  await getUsers()
  loadingStore.fisnish()

}
async function deleteUser(user:User){
  loadingStore.doload()
  const res = await userService.delUser(user)
  await getUsers()
    loadingStore.fisnish()
}
  return { users,saveUser,getUsers,deleteUser,getUser}
})
