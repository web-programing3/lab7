import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('loading', () => {
  const isloading = ref(false)
 const doload = ()=>{
    isloading.value = true
 }
const fisnish = ()=>{
    isloading.value = false
}
  return { isloading,doload,fisnish}
})
